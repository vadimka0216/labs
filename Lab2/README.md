# Название 
Сбор и аналитическая обработка информации о сетевом трафике

## Цель
1. Развить практические навыки использования современного стека инструментов сбора и аналитической обработки информации о сетвом трафике
2. Освоить базовые подходы блокировки нежелательного сетевого трафика
3. Закрепить знания о современных сетевых протоколах прикладного уровня

## Исходные данные
1. Ноутбук на windows 10 pro
2. Программа Wireshark
3. Zeek
4. Python 3.7

### # Общий план выполнения

1. Собрать сетевой трафик (объемом не менее 100 Мб)
2. Выделить метаинформацию сетевого трафика с помощью утилиты Zeek
3. Собрать данные об источниках нежелательного трафика, например – https://github.com/StevenBlack
4. Написать программу на любом удобном языке (python, bash, R …), котрая подсчитывает процент нежелательного трафика в собранном на этапе 1.

# Содержание ЛБ
## Шаг 1 Собрать сетевой трафик (объемом не менее 100 Мб)
Воспользуемся утилитой Wireshark для сбора сетевого трафика. Собирать трафик будем на интерфейсе eth0.
![Выбор интерфейса eth0 ](https://i.imgur.com/XBN3Foi.png "Орк")

Собираем трафик переходя на новостные сайты
 
![Собираем трафик ](https://i.imgur.com/nhIKqd0.png "Орк")

Сохраняем полученный файл

![Сохраняем файл ](https://i.imgur.com/ku68uS4.png "Орк")

## Шаг 2 Выделить метаинформацию с утилитой Zeek
Запускаем Zeek с сохраненным нами ранее файлом и получаем распределенные данные о трафике 

![Результат выполнения Zeek ](https://i.imgur.com/5cKPUoC.png "Орк")

## Шаг 3 Собрать данные об источниках нежелательного трафика, например – https://github.com/StevenBlack

Скопируем содержимое из репозитория https://github.com/StevenBlack , удалим ненужную информацию и получим 93431 нежелательных хостов. 
![Нежелательные хосты ](https://i.imgur.com/Yw4lb0m.png "Орк")

## Шаг 4 Написать программу на любом удобном языке (python, bash, R …), котрая подсчитывает процент нежелательного трафика в собранном на этапе 1.
```
import csv
import re

dnslog = open("dns.log");
traff = open('badhosts.txt');
dnstraff = csv.reader(dnslog, delimiter="\x09");
malvare_count = malv_det = 0;
reg = re.compile('[^a-zA-Z.]');
hosts=[];

for line in traff.readlines():
	hosts.append(reg.sub('', line));

for row in dnstraff:
	try:
		malvare_count+=1;
		if row[9] in hosts:
			print(row[9]);
			malv_det+=1;
	except IndexError:
		pass

print("The percentage of unwanted traffic is "+str(malv_det * 100.0 / malvare_count) + "%");  


dnslog.close()
traff.close()
```
## Результат
![Вывод резульатат ](https://i.imgur.com/t9HuWi9.png "Орк")

## Вывод
При выполнении практической работы были развиты практические навыки использования современного стека инструментов сбора и аналитической обработки информации о сетвом трафике, а также освоены базовые подходы блокировки нежелательного сетевого трафика.
